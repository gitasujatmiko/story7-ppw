from django.db import models

class Status(models.Model):

    def __str__(self):
        return "Status by " + self.nama

    nama = models.CharField(max_length=100)
    status = models.TextField()

