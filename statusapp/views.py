from django.shortcuts import render, redirect
from django.urls import reverse
from .forms import StatusForm
from .models import Status

def index(request):
    
    form = StatusForm(request.POST or None)
    all_status = Status.objects.all()

    context = {
        'form': form, 
        'all_status': all_status
    }

    if form.is_valid():
        nama = request.POST.get('nama')
        status = request.POST.get('status')
        return redirect('statusapp:status_confirmation', nama, status)


    return render(request, 'statusapp/index.html', context)

def status_confirmation(request, nama, status):

    context = {
        'nama': nama,
        'status': status,
    }

    if request.method == 'POST':
        Status.objects.create(nama=nama, status=status)
        return redirect('statusapp:index')

    return render(request, 'statusapp/status-confirmation.html', context)
