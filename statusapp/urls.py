from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'statusapp'

urlpatterns = [
    path('', views.index, name='index'),
    path('status-confirmation/<str:nama>/<str:status>', views.status_confirmation, name='status_confirmation'),
]