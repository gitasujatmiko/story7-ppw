from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Status
from .forms import StatusForm
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

class Story7UnitTest(TestCase):

    def test_story7_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab5_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        # Creating a new status
        new_status = Status.objects.create(nama='Anonymous', status='Abdi teh isin ari teu anonim mah.')

        # Retrieving all status
        counting_all_status = Status.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'nama': '', 'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

    def test_status_post_success(self):
        test = 'Anonymous'
        response_post = Client().post('/', {'nama': test, 'status': test})
        self.assertEqual(response_post.status_code, 302)
    
    def test_status_post_error(self):
        test = 'Anonymous'
        response_post = Client().post('/', {'nama': '', 'status': ''})
        self.assertEqual(response_post.status_code, 200)

        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

class SeleniumTestCase(LiveServerTestCase):

    def setUp(self):
        # ChromeDriver 81.0.4044.69
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(SeleniumTestCase, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(SeleniumTestCase, self).tearDown()

    def test_status_post_is_confirmed_with_selenium(self):
        driver = self.selenium
        driver.get(self.live_server_url)
        assert 'Status' in driver.title

        nama = driver.find_element_by_id('id_nama')
        status = driver.find_element_by_id('id_status')
        submit = driver.find_element_by_tag_name('button')
        nama.send_keys('Anonymous')
        status.send_keys('Hehehehehe')
        submit.send_keys(Keys.RETURN)

        confirm = driver.find_element_by_tag_name('button')
        confirm.send_keys(Keys.RETURN)

        assert 'Anonymous' in driver.page_source
        assert 'Hehehehehe' in driver.page_source

    def test_status_post_is_canceled_with_selenium(self):
        driver = self.selenium
        driver.get(self.live_server_url)
        assert 'Status' in driver.title

        nama = driver.find_element_by_id('id_nama')
        status = driver.find_element_by_id('id_status')
        submit = driver.find_element_by_tag_name('button')
        nama.send_keys('YeAy')
        status.send_keys('1234')
        submit.send_keys(Keys.RETURN)

        confirm = driver.find_element_by_css_selector('a.btn-danger')
        confirm.send_keys(Keys.RETURN)

        assert 'YeAy' not in driver.page_source
        assert '1234' not in driver.page_source