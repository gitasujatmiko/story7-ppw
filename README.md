# Tugas Story 7-10 PPW
Gita Permatasari Sujatmiko (1906400053)  
[Click here to access the website for Story 7](https://gpersable-ppw.herokuapp.com/)  
[Click here to access the website for Story 8](https://gpersable-ppw.herokuapp.com/story8/)  
[Click here to access the website for Story 9](https://gpersable-ppw.herokuapp.com/story9/)  
[Click here to access the website for Story 10](https://gpersable-ppw.herokuapp.com/story10/)

[![pipeline status](https://gitlab.com/gitasujatmiko/story7-ppw/badges/master/pipeline.svg)](https://gitlab.com/gitasujatmiko/story7-ppw)

[![coverage report](https://gitlab.com/gitasujatmiko/story7-ppw/badges/master/coverage.svg)](https://gitlab.com/gitasujatmiko/story7-ppw)
