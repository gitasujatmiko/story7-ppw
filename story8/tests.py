from django.test import TestCase, Client
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions as EC

import time

class UnitTest(TestCase):

    def test_story8_using_the_right_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8/index.html')

class SeleniumTestCase(LiveServerTestCase):

    def setUp(self):
        # ChromeDriver 81.0.4044.69
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(SeleniumTestCase, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(SeleniumTestCase, self).tearDown()

    def test_accordion_when_clicked(self):
        driver = self.selenium
        driver.get(self.live_server_url + '/story8/')
        assert 'Story' in driver.title

        aktivitas = driver.find_element_by_xpath("//button[1]")
        aktivitas.click()
        assert '#dirumahaja' in driver.page_source

        up = driver.find_element_by_xpath("//a[3]")
        down = driver.find_element_by_xpath("//a[4]")
        up.click()
        down.click()

    def test_change_theme(self):
        driver = self.selenium
        driver.get(self.live_server_url + '/story8/')

        assert 'id="switch"' in driver.page_source

        switch_theme_button = driver.find_element_by_css_selector('button#switch')

        assert 'data-theme="dark"' not in driver.page_source
        switch_theme_button.click()
        # assert 'data-theme="dark"' in driver.page_source
