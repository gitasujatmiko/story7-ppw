from django.contrib import admin
from django.urls import path, re_path, include
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.index, name='index'),
    path('register/', views.register, name='register'),
    path('', include('django.contrib.auth.urls')),
    path('profile/', views.user_profile, name='profile'),
    path('myprofile/', views.my_profile, name='myprofile'),
]