from django.db import models
from django import forms
from django.db.models.signals import post_save
from django.contrib.auth.models import AbstractBaseUser
from django.conf import settings
from django.contrib.auth.models import UserManager
from django.contrib.auth import get_user_model

User = get_user_model()

class UserProfile(AbstractBaseUser):

    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female'),
    )

    REQUIRED_FIELDS = ('user',)
    USERNAME_FIELD = 'username'

    user = models.OneToOneField(User, related_name='myprofile', on_delete=models.CASCADE)

    photo = models.URLField(null=True)
    username = models.CharField(max_length=50, unique=False)
    first_name = models.CharField(max_length=50, null=True)
    last_name = models.CharField(max_length=50, null=True)
    gender = models.CharField(max_length=10, choices=GENDER_CHOICES, null=True)
    website = models.URLField(null=True)

    objects = UserManager()

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)