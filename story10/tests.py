from django.test import TestCase
from django.test import TestCase, Client
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions as EC

class UnitTest(TestCase):

    def test_story10_using_the_right_template(self):
        response = Client().get('/story10/')
        self.assertTemplateUsed(response, 'story10/index.html')

    def test_registerpage_using_the_right_template(self):
        response = Client().get('/story10/register/')
        self.assertTemplateUsed(response, 'register/register.html')

    def test_myprofile_using_the_right_template(self):
        response = Client().get('/story10/myprofile/')
        self.assertTemplateUsed(response, 'story10/myprofile.html')

class SeleniumTestCase(LiveServerTestCase):

    def setUp(self):
        # ChromeDriver 81.0.4044.69
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(SeleniumTestCase, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(SeleniumTestCase, self).tearDown()

    def test_story10_signup_and_login(self):
        driver = self.selenium
        driver.get(self.live_server_url + '/story10/register/')
        assert 'Story 10 | Signup' in driver.title

        username1 = driver.find_element_by_xpath("//input[@name='username']")
        username1.send_keys('user001')

        password1 = driver.find_element_by_xpath("//input[@name='password1']")
        password1.send_keys('nyobanyoba')

        password2 = driver.find_element_by_xpath("//input[@name='password2']")
        password2.send_keys('nyobanyoba')

        signup = driver.find_element_by_tag_name('button')
        signup.click()

        login = driver.find_element_by_xpath("//a[@name='login']")
        login.click()

        username2 = driver.find_element_by_xpath("//input[@name='username']")
        username2.send_keys('user001')

        password = driver.find_element_by_xpath("//input[@name='password']")
        password.send_keys('nyobanyoba')

        login_button = driver.find_element_by_tag_name('button')
        login_button.click()

        edit_button = driver.find_element_by_xpath("//a[@name='profile']")
        edit_button.click()

        photourl = driver.find_element_by_xpath("//input[@name='photo']")
        photourl.send_keys('https://i.ibb.co/Jn7dxWJ/11817155-504086109766959-5987435817977834128-n.jpg')

        first_name = driver.find_element_by_xpath("//input[@name='first_name']")
        first_name.send_keys('Gita')

        last_name = driver.find_element_by_xpath("//input[@name='last_name']")
        last_name.send_keys('Sujatmiko')

        select = Select(driver.find_element_by_name('gender'))
        select.select_by_visible_text('Female')

        website = driver.find_element_by_xpath("//input[@name='website']")
        website.send_keys('https://gitasujatmiko.herokuapp.com/')

        update_button = driver.find_element_by_xpath("//button[@name='update_profile']")
        update_button.click()

        assert 'user001' in driver.page_source