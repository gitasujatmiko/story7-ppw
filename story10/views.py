from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.http import Http404
from .forms import UserProfileForm
from .models import UserProfile
from django.contrib.auth import get_user_model

User = get_user_model()

def index(request):
    return render(request, 'story10/index.html')

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            user = form.save(commit=False)
            user.save()
            messages.success(request, f'Welcome {username}, your account is created')
            
            return redirect('story10:index')
    else:
        form = UserCreationForm()
    return render(request, 'register/register.html', {'form':form})

def user_profile(request):
    if request.user.is_authenticated:
        # profile = request.user.UserProfile
        user_instance = UserProfile.objects.filter(user=request.user).first()
        user_profile_form = UserProfileForm(request.POST, request.FILES, instance=user_instance or None)
        if user_profile_form.is_valid():
            profile = user_profile_form.save(commit=False)
            profile.user = request.user
            profile.save()
            return redirect('story10:myprofile')
        return render(request, 'story10/profile.html', {'user_profile_form': user_profile_form})
    raise Http404()

def my_profile(request):
    return render(request, 'story10/myprofile.html')