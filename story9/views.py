from django.shortcuts import render
from django.http import JsonResponse, Http404
from urllib.request import urlopen
import requests
import json

def index(request):
    # if request.is_ajax():
    #     response = requests.get(
    #             "https://www.googleapis.com/books/v1/volumes?q={}".format(
    #                 request.GET["q"]
    #             ))
    #     bookid = response.json()['items']['id']
    #     return render(request, 'story9/index.html', {'book_id': bookid})
    return render(request, 'story9/index.html')

def list_of_books(request):
    if request.is_ajax():
        response = requests.get(
            "https://www.googleapis.com/books/v1/volumes?q={}".format(
                request.GET["q"]
            ))
        return JsonResponse(response.json())
    raise Http404()

def like_book(request, bookid):
    if request.is_ajax():
        return render(request, 'story9/index.html', {'bookid':bookid})
    raise Http404()
