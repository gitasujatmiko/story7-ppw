from django.test import TestCase, Client
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions as EC

import time

class UnitTest(TestCase):

    def test_story9_using_the_right_template(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9/index.html')

class SeleniumTestCase(LiveServerTestCase):

    def setUp(self):
        # ChromeDriver 81.0.4044.69
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(SeleniumTestCase, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(SeleniumTestCase, self).tearDown()

    def test_cari_buku(self):
        driver = self.selenium
        driver.get(self.live_server_url + '/story9/')
        assert 'Books' in driver.title

        caribuku = driver.find_element_by_tag_name('input')
        caribuku.send_keys('IPA')

        cari = driver.find_element_by_css_selector('button.btn-primary')
        cari.click()

        assert 'IPB' not in driver.page_source