from django.contrib import admin
from django.urls import path, re_path, include
from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.index, name='index'),
    path('books', views.list_of_books, name='booklist'),
    re_path(r'^likebook/((?P<bookid>[A-Za-z0-9])|\s)', views.like_book, name='likebook'),
]