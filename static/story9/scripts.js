$(function () {
    function get_books(query) {
        $("#books").empty()

        $.ajax({
            url: book_url + query,
            type: "GET",
            success: function (result) {
                $.each(result.items, function (i) {
                    result.items[i].likeCount = 0;
                    var item = result.items[i]["volumeInfo"]
                    $("#books").append(
                        $("<div/>")
                        .addClass("row mx-0")
                        .append( // image
                            $("<div/>").addClass("col-6 col-md-2 pl-0 mb-4").html(
                                function (item) {
                                    if (item.imageLinks !== undefined) {
                                        return $("<img/>").attr("src", "https" + item.imageLinks.smallThumbnail.slice(4))
                                    }
                                    return ""
                                }(item)
                            )
                        )
                        .append( // title
                            $("<div/>").addClass("col-6 col-md-2 p-2 mb-4").html(
                                $("<a/>")
                                .attr("target", "_blank")
                                .attr("href", item.canonicalVolumeLink)
                                .text(item.title)
                            )
                        )
                        .append( // authors
                            $("<div/>").addClass("col-6 col-md-2 p-2 mb-4").html(
                                function (item) {
                                    if (item.authors !== undefined) {
                                        var $ul = $("<ul/>")
                                        $.each(item.authors, function (i) {
                                            $ul.append(
                                                $("<li/>").text(item.authors[i])
                                            )
                                        })
                                        return $ul
                                    }
                                    return ""
                                }(item)
                            )
                        )
                        .append( // publisher
                            $("<div/>").addClass("col-6 col-md-2 p-2 mb-4").text(item.publisher)
                        )
                        .append( // likes
                            $("<div/>").addClass("col-6 col-md-2 p-2 mb-4 likes").html("<a href='#' id='like'>Like</a><br><a id='likecount'>0</a>")
                        ),

                    )

                    $('#like').click(function (e) {
                        var bookid = result.items[e]["id"]
                        $.ajax({
                            type: "POST",
                            url: "/likebook/" + bookid,
                            data: {
                                'bookid': bookid,
                                'csrfmiddlewaretoken': csrftoken,
                            },
                            success: function (response) {
                                
                                var count = $('#likecount').html();
                                $('#likecount').html(parseFloat(count) + 1);
                            },
                            error: function (rs, e) {
                                console.log(rs)
                                console.log(e)
                            }
                        });
                    });

                    $("#books").attr("data-search", query);
                })
            },
            error: function (rs, e) {
                console.log(rs)
                console.log(e)
            }
        })

        
    }

    

    $("button#search").click(function () {
        get_books($('input[name=q]').val())
    })

    $("input[name=q]").keypress(function (e) {
        var key = e.which;
        // enter key
        if (key == 13) {
            $("button#search").click();
        }
    });

    

    if ($("input[name=q]").val() !== "") {
        $("button#search").click();
    } else {
        get_books("A");
    }


})