// $(function () {
//     $('.section').accordion({
//         collapsible: true, active: false
//     });

//     $(document).click(function(e) {
//         if(e.target.className === 'down' || e.target.className === 'up') {
//             $('.section').accordion({
//                 active: false
//             }); 
//         }
//     })

//     $('.up').click(function () {
//         $(this).parents('.section').insertBefore($(this).parents('.section').prev());
//     });

//     $('.down').click(function () {
//         $(this).parents('.section').insertAfter($(this).parents('.section').next());
//     });
// });

var acc = $('.accordion');
var i;

$(function() {

    for (i = 0; i < acc.length; i++) {
        $(acc[i]).click(function () {
            this.classList.toggle("active");
            var updown = this.nextElementSibling;
            var panel = updown.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
    
    $('.up').click(function () {
        $(this).parents('.section').insertBefore($(this).parents('.section').prev());
    });
    
    $('.down').click(function () {
        $(this).parents('.section').insertAfter($(this).parents('.section').next());
    });

});

// document.getElementById('switch').onclick = function() {
//     if (document.getElementById('theme').href == normalTheme) {
//       document.getElementById('theme').href = otherTheme;
//     } else {
//       document.getElementById('theme').href = normalTheme;
//     }
//   };

$('#switch').click(function() {
    if ($('html')[0].hasAttribute('data-theme')) {
        $('html')[0].removeAttribute('data-theme');
        return
    }
    $('html')[0].setAttribute('data-theme', 'dark');
});
